import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';
import Home from './Containers/Home';
import 'firebase/auth';
import * as serviceWorker from './serviceWorker';

const container = document.getElementById('home-container');
ReactDOM.render(<Home />, container);

const auth = firebase.auth();

export {
  auth,
};

serviceWorker.unregister();
