import React from 'react'
import "./login-page.css"

const Login = (props)=>(
	<div className="login-container">
        <div className="form-container">
            <h2 className="title-login">Login</h2>
            <form onSubmit={props.handleSubmit} className="form">
                <label>
                    <span className="extend">Email:</span>
                    <input
                        className="input"
                        type="email"
                        placeholder="Enter your Email"
                        name="email"
                        ref={props.setRefE}
                        value={props.email}
                        onChange={props.handleChange}
                    />
                </label>
                <label>
                    <span className="extend">Password:</span>
                    <input
                        className="input"
                        type="password"
                        placeholder="Enter your Password"
                        name="password"
                        ref={props.setRefP}
                        value={props.pass}
                        onChange={props.handleChange}
                    />
                </label>

                <input
                    className="input-submit"
                    type="submit"
                    name="submit"
                    //ref={props.setRef}
                    value='Login'
                    //onChange={props.handleChange}
                />
            </form>
        </div>
	</div>
)
export default Login;