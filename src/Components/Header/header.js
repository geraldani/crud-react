import React from 'react';
import { Link } from 'react-router-dom';
import './header.css'

import * as routes from '../../Constants/routes';

const Header = () =>
  <header className="header-container">
	  <div>
	  	<h1 className="title">Crud Administration Web App</h1>	
	  </div>

	  <div>
	    <ul>
	      <li><Link to={routes.LOGIN}>Sign In</Link></li>
	      <li><Link to={routes.REGISTER}>Sign Up</Link></li>
	    </ul>
	  </div>
  </header>


export default Header