import React from 'react'
import './register.css';

const Register = (props)=>(
	<div className="register-container">
        <div className="form-container">
            <h2 className="title-register">Register</h2>
            <form onSubmit={props.handleSubmit} className="form">
                <label>
                    <span className="extend">Name:</span>
                    <input
                        className="input"
                        type="text"
                        placeholder="Enter your Name"
                        name="name"
                        value={props.name}
                        onChange={props.handleChange}
                        ref={props.refName}
                    />
                </label>

                <label>
                    <span className="extend">Lastname:</span>
                    <input
                        className="input"
                        type="text"
                        placeholder="Enter your Lastname"
                        name="lastname"
                        ref={props.refSurname}
                        value={props.surname}
                        onChange={props.handleChange}
                    />
                </label>

                <label>
                    <span className="extend">Email:</span>
                    <input
                        className="input"
                        type="email"
                        placeholder="Enter your Email"
                        name="email"
                        ref={props.refEmail}
                        value={props.email}
                        onChange={props.handleChange}
                    />
                </label>

                <label>
                    <span className="extend">Password:</span>
                    <input
                        className="input"
                        type="password"
                        placeholder="Enter your Password"
                        name="password"
                        ref={props.refPass}
                        value={props.pass}
                        onChange={props.handleChange}
                    />
                </label>

                <input
                    className="input-submit"
                    type="submit"
                    name="submit"
                    value='Register'
                />
            </form>
        </div>
    </div>
)
export default Register;