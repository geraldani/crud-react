import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";
//import { Navbar } from "react-bootstrap";
import * as routes from "../Constants/routes";
import './Home.css';
import Header from '../Components/Header/header';
import RegisterContainer from './Register/register-container';
import LoginContainer from './Login/login-container';


class Home extends Component {
  render() {
    return (
    	<Router>
    		<div>
				<Header />
				<Route exact path={routes.LOGIN} component={LoginContainer} />
				<Route exact path={routes.REGISTER} component={RegisterContainer} />
			</div>
    	</Router>
    );
  }
}

export default Home;
