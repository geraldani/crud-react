import React, { Component } from "react";
import Register from '../../Components/Register/register-page';

class RegisterContainer extends Component{
	constructor(props) {	
		super(props);
		this.state = {
			name: "",
			surname: "",
			email: "",
			password: ""
		};
	}

	handleInputChange = (evento) => {
        this.setState({
            name: this.inputName.value,
            surname: this.inputSurname.value,
            email: this.inputEmail.value,
            password: this.inputPass.value
        });
    };

	handleSubmit = event => {
		event.preventDefault();
	}

	render(){
		return (
			<Register
				handleSubmit={this.handleSubmit}
				handleChange={this.handleInputChange}
				email={this.state.email}
				pass={this.state.password}
				name={this.state.name}
				surname={this.state.surname}
				refEmail={ element => this.inputEmail = element}
				refName={ element => this.inputName = element}
				refSurname={ element => this.inputSurname = element}
				refPass={ element => this.inputPass = element}
			/>
		)
	}
}

export default RegisterContainer;