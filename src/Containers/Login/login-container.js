import React, { Component } from "react";
//import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import Login from '../../Components/Login/login-page';

class LoginContainer extends Component{
	constructor(props) {	
		super(props);
		//initial state
		this.state = {
			email: "",
			password: ""
		};
	}

	validateForm() {
		return this.state.email.length > 0 && this.state.password.length > 0;
	}

	handleChange = event => {
		this.setState({
            email: this.inputEmail.value,
            password: this.inputPass.value
        });
	}

	handleSubmit = event => {
		event.preventDefault();
	}

	render(){
		return (
			<Login
				handleSubmit={this.handleSubmit}
				handleChange={this.handleChange}
				setRefE={ elementoHTML => this.inputEmail = elementoHTML}
				setRefP={ elementoHTML => this.inputPass = elementoHTML}
				email={this.state.email}
				pass={this.state.password}
			/>
		)
	}
}

export default LoginContainer;